package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"
)

const (
	Black   = "\033[1;30m%s\033[0m"
	Red     = "\033[1;31m%s\033[0m"
	Green   = "\033[1;32m%s\033[0m"
	Yellow  = "\033[1;33m%s\033[0m"
	Purple  = "\033[1;34m%s\033[0m"
	Magenta = "\033[1;35m%s\033[0m"
	Teal    = "\033[1;36m%s\033[0m"
	White   = "\033[1;37m%s\033[0m"
)

func main() {
	verify()
}

func verify() {
	args := os.Args[1:]
	if len(args) < 2 {
		fmt.Printf(Yellow, "Please provide argument project path by using runner in-scope env var $CI_PROJECT_PATH")
		fmt.Println("")
		os.Exit(1)
	}
	fmt.Println("Provided path: " + args[0])
	fmt.Printf(Magenta, "Test Started - Submission Date")
	fmt.Println("")
	arr := strings.Split(args[0], "/")
	if len(arr) < 3 {
		fmt.Printf(Red, "Error while parsing env var $CI_PROJECT_PATH, it might be due to gitlab update of env vars names/values link: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html")
		fmt.Println("")
		os.Exit(1)
	}
	exerciseName := arr[len(arr)-1]
	studentName := arr[len(arr)-2]
	className := arr[len(arr)-3]
	fmt.Printf(Yellow, "********************************\n")
	fmt.Printf(Yellow, "Getting project information ... ")
	fmt.Printf(Yellow, "\n********************************\n")
	fmt.Println("")
	fmt.Printf("ExerciseName: %v\nStudentName: %v\nClassName: %v\n", exerciseName, studentName, className)
	files, e := os.ReadDir("./")
	committerID := args[1]
	fmt.Println("Provided committer ID: " + committerID)
	if !strings.Contains(studentName, committerID) {
		fmt.Printf(Green, "Committer is instructor, skipping submission date verification...")
		fmt.Println("")
		return
	}
	if e != nil {
		fmt.Printf(Red, "Error while reading dir content")
		fmt.Println("")
		os.Exit(1)
	}
	splt := strings.Split(exerciseName, "_")
	week := splt[len(splt)-1]
	week = strings.ReplaceAll(strings.ToLower(week), "ex", "")
	for _, file := range files {
		if !strings.Contains(file.Name(), fmt.Sprint(time.Now().Year())) {
			continue
		}
		bytes, err := os.ReadFile(file.Name())
		if err != nil {
			fmt.Printf(Red, fmt.Sprintf("error occurred during attempt to read file [%v] %v, skipping file", file.Name(), err.Error()))
			continue
		}
		bytes = []byte(strings.ToLower(string(bytes)))
		dueDates := map[string]map[string]map[string]string{}
		err = json.Unmarshal(bytes, &dueDates)
		if err != nil {
			fmt.Printf(Red, fmt.Sprintf("error occurred during attempt to unmarshal file [%v] content to json format %v, skipping file", file.Name(), err.Error()))
			continue
		}
		classWeeks, exists := dueDates[className]
		if !exists {
			fmt.Printf(Red, fmt.Sprintf("class [%v] was not set yet, no deadlines dates available currently, skipping", className))
			continue
		}
		dates, exists := classWeeks[week]
		if !exists {
			fmt.Printf(Red, fmt.Sprintf("class [%v] exercise [%v] was not set yet, no deadline date available currently, skipping", className, week))
			continue
		}
		dueToKey := "dueto"
		dueDate, exists := dates[dueToKey]
		if !exists {
			fmt.Printf(Red, fmt.Sprintf("key [%v] does not exists in json, however is expected by test, please fix target json format\n", dueToKey))
			continue
		}
		fmt.Printf("week [%v] due date [%v]\n", week, dueDate)
		date, e := time.Parse("2006-01-02", dueDate)
		if e != nil {
			fmt.Printf(Red, "Date is not defined with expected form YYYY-MM-DD")
			fmt.Println("")
			os.Exit(1)
		}
		if time.Now().Before(date) {
			fmt.Printf(Green, "Submission ok :)")
			fmt.Println("")
			return
		} else {
			fmt.Printf(Red, "Submission is late :(")
			fmt.Println("")
			os.Exit(1)
		}
	}
	fmt.Printf(Red, fmt.Sprintf("could not find any relevant dates checker file for new magshimim year [%v]", time.Now().Year()))
	os.Exit(1)
}
